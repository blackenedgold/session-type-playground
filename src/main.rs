use session_types::*;
use std::thread;

type Server = Rec<Recv<i64, Send<bool, Choose<Var<Z>, Eps>>>>;
type Client = <Server as HasDual>::Dual;

fn srv(c: Chan<(), Server>) {
    let mut c = c
        // Rec<Recv<i64, Send<bool, Choose<Var<Z>, Eps>>>> -> Recv<i64, Send<bool, Choose<Var<Z>, Eps>>>
        // 環境にラベルを追加
        .enter();
    loop {
        let (c_, n) = c.
            // Recv<i64, Send<bool, Choose<Var<Z>, Eps>>> -> Send<bool, Choose<Var<Z>, Eps>>
            recv();
        if 100 <= n {
            c = c_
                // Send<bool, Choose<Var<Z>, Eps>> -> Choose<Var<Z>, Eps>
                .send(true)
                // Choose<Var<Z>, Eps> -> Var<Z>
                .sel1()
                // Var<Z> -> Recv<i64, Send<bool, Choose<Var<Z>, Eps>>>
                .zero();
        } else {
            c_
                // Send<bool, Choose<Var<Z>, Eps>> -> Choose<Var<Z>, Eps>
                .send(false)
                // Choose<Var<Z>, Eps> -> Eps
                .sel2()
                // Eps → 終了
                .close();
            break;
        }
    }
}

fn cli(c: Chan<(), Client>) {
    let mut c = c
        // Rec<Send<i64, Recv<bool, Offer<Var<Z>, Eps>>>> -> Send<i64, Recv<bool, Offer<Var<Z>, Eps>>>
        // 環境にラベルを追加
        .enter();
    for n in &[101, 100001, 42, 200] {
        let c_ = c
            // Send<i64, Recv<bool, Offer<Var<Z>, Eps>>> -> Recv<bool, Offer<Var<Z>, Eps>>
            .send(*n);
        let (c_, b) = c_
            // Recv<bool, Offer<Var<Z>, Eps>> -> Offer<Var<Z>, Eps>
            .recv();
        if b {
            println!("{} is big", n);
        } else {
            println!("{} is small", n);
        }
        c = match c_
            // Offer<Var<Z>, Eps> ->
            .offer()
        {
            // Var<Z>
            Left(c) => c
                // Var<Z> -> Send<i64, Recv<bool, Offer<Var<Z>, Eps>>>
                .zero(),
            // Eps
            Right(c) => {
                println!("server stopped");

                c.close();
                break;
            }
        };
    }
}

fn main() {
    let (server_chan, client_chan) = session_channel();

    let srv_t = thread::spawn(move || srv(server_chan));
    let cli_t = thread::spawn(move || cli(client_chan));

    let _ = (srv_t.join(), cli_t.join());
}
